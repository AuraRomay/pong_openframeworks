#pragma once

class Jugador {
public:
	int altura;
	int ancho;
	float posicionX;
	float posicionY;
	bool teclaUp;
	bool teclaDown;
	int punto;

	Jugador();
	Jugador(int height, int width, float posX, float posY);
};
