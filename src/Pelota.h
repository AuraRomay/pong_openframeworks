#pragma once

class Pelota {
public:
	float x;
	float y;
	float rad;
	float vx;
	float vy;
	
	Pelota();
	void init(float x, float y, float r);
	void move();
	void bounceX();
	void bounceY();
	void draw();
};