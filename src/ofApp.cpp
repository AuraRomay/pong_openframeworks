#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	player1 = Jugador(120, 30, 20, ofGetHeight()/2);
	player2 = Jugador(120, 30, ofGetWidth() - 50, ofGetHeight() / 2);
	player1.punto = 0;
	player2.punto = 0;
	pelotita.init(ofGetWidth() / 2, ofGetHeight() / 2, 20);
}

//--------------------------------------------------------------
void ofApp::update(){

	pelotita.move();

	//Jugador 2
	if (player2.posicionY <= 0) {
		player2.posicionY = 0;
	}
	if (player2.posicionY + player2.altura >= ofGetHeight()) {
		player2.posicionY = ofGetHeight() - player2.altura;
	}

	if (player2.teclaUp == true) {
		player2.posicionY -= 1;
	}
	if (player2.teclaDown == true) {
		player2.posicionY += 1;
	}

	//Jugador 1
	if (player1.posicionY <= 0) {
		player1.posicionY = 0;
	}
	if (player1.posicionY + player1.altura >= ofGetHeight()) {
		player1.posicionY = ofGetHeight() - player1.altura;
	}

	if (player1.teclaUp == true) {
		player1.posicionY -= 1;
	}
	if (player1.teclaDown == true) {
		player1.posicionY += 1;
	}
	
	//Pelota rebotes con pared
	if (pelotita.x <= 0) {
		/*pelotita.bounceX();
		pelotita.x = 1;*/
		//cout << "TOCA1" << endl;
		player2.punto += 1;
		pelotita.x = ofGetWidth() / 2;
		pelotita.y = ofGetHeight() / 2;
	}

	if (pelotita.x >= ofGetWidth()) {
		/*pelotita.bounceX();
		pelotita.x = ofGetWidth() - 1;*/
		//cout << "TOCA2" << endl;
		player1.punto += 1;
		pelotita.x = ofGetWidth() / 2;
		pelotita.y = ofGetHeight() / 2;
	}

	if (pelotita.y <= 0) {
		pelotita.bounceY();;
		pelotita.y = 1;
	}

	if (pelotita.y >= ofGetHeight()) {
		pelotita.bounceY();
		pelotita.y = ofGetHeight() - 1;
	}

	//Rebotes con los jugadores
	if (pelotita.x > player1.posicionX && pelotita.x <= (player1.posicionX + player1.ancho)
		&& pelotita.y > player1.posicionY && pelotita.y <= (player1.posicionY + player1.altura)){
		pelotita.bounceX();
	}

	if (pelotita.x > player2.posicionX && pelotita.x <= (player2.posicionX + player2.ancho)
		&& pelotita.y > player2.posicionY && pelotita.y <= (player2.posicionY + player2.altura)) {
		pelotita.bounceX();
	}
	/*pelotita.x += pelotita.vx * ofGetLastFrameTime();
	pelotita.y += pelotita.vy * ofGetLastFrameTime();*/
}

//--------------------------------------------------------------
void ofApp::draw(){

	ofSetColor(ofColor::red);
	ofDrawRectangle(player1.posicionX, player1.posicionY, player1.ancho, player1.altura);
	ofSetColor(ofColor::blue);
	ofDrawRectangle(player2.posicionX, player2.posicionY, player2.ancho, player2.altura);
	pelotita.draw();
	ofSetColor(ofColor::black);
	ofDrawBitmapString(ofToString(player1.punto), 450, 100);
	ofDrawBitmapString(ofToString(player2.punto), 550, 100);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == 'w' || key == 'W') {
		player1.teclaUp = true;
	}
	if (key == 's' || key == 'S') {
		player1.teclaDown = true;
	}

	if (key == OF_KEY_UP) {
		player2.teclaUp = true;
	}
	if (key == OF_KEY_DOWN) {
		player2.teclaDown = true;
	}

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (key == 'w' || key == 'W') {
		player1.teclaUp = false;
	}

	if (key == 's' || key == 'S') {
		player1.teclaDown = false;
	}

	if (key == OF_KEY_UP) {
		player2.teclaUp = false;
	}
	if (key == OF_KEY_DOWN) {
		player2.teclaDown = false;
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}