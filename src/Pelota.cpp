#include "Pelota.h"
#include "ofGraphics.h"
#include "ofMath.h"	//Para Random
#include "ofAppRunner.h"	//
#include "ofUtils.h"	//Para usar oftostring

Pelota::Pelota() {
	x = ofRandom(ofGetWidth());
	y = ofRandom(ofGetHeight());
	rad = ofRandom(10, 30);
	vx = 1;
	vy = 1;
}

void Pelota::init(float x, float y, float r) {
	this->x = x;
	this->y = y;
	this->rad = r;
	vx = 1;
	vy = 1;
}

void Pelota::bounceX() {
	vx *= -1;
}

void Pelota::bounceY() {
	vy *= -1;
}

void Pelota::move() {
	x += vx;
	y += vy;
}

void Pelota::draw() {
	/*ofSetColor(0);
	ofNoFill();
	ofDrawCircle(x, y, rad);*/
	ofSetColor(ofColor::coral);
	ofFill();
	ofDrawCircle(x, y, rad);
	//ofSetColor(0);
	//ofDrawBitmapString(ofToString(index), x, y);
}